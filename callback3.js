// const fs = require("fs");
// const path = require("path");
// function problem3(listsID, callback3) {
//     setTimeout(() => {
//         fs.readFile(path.join(__dirname, "./data/cards.json"), (error, data) => {
//             if (error) {
//                 console.log(error)
//             } else {
//                 let result = JSON.parse(data)

//                 callback3(null,result[listsID])
//             }

//         })
//     }, 2 * 1000)

// }

// module.exports = problem3

const fs = require("fs");
const path = require("path");
function RetrieveData(idArray, data) {
  fs.readFile(path.join(__dirname, data), (err, data) => {
    if (err) {
      err = new Error("error while reading");
    } else {
      let dataIds = JSON.parse(data).employees.filter((data) => {
        return idArray.includes(data.id);
      });
      console.log(dataIds);
    }
  });
  fs.writeFile(
    path.join(__dirname, "output.json"),
    JSON.stringify(dataIds),
    (err, data) => {
      if (err) {
        err = new Error("error writing file");
      } else {
        console.log("file written");
      }
    }
  );
}
RetrieveData([1], "data.json");
