const fs = require("fs");
const path = require("path");
function problem2(passID, callback2) {
    setTimeout(() => {
        fs.readFile(path.join(__dirname, "./data/lists.json"), (error, data) => {
            if (error) {
                console.log(error)
            } else {
                let result = JSON.parse(data)

                callback2(null,result[passID])
            }

        })
    }, 2 * 1000)
}


module.exports = problem2

