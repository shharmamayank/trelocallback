const fs = require("fs");
const path = require("path");
const dataFromCallbackOne = require("./callback1")
const dataFromCallbackTwo = require("./callback2")
const dataFromCallbackthree = require("./callback3")

function problem4(name) {
    setTimeout(() => {
        fs.readFile(path.join(__dirname, "./data/boards.json"), (error, data) => {
            if (error) {
                console.log(error)
            } else {
                let result = JSON.parse(data).filter((index) => {
                    if (index.name === name) {
                        dataFromCallbackOne(index.id, (err, data) => {
                            if (err) {
                                console.log(err)
                            } else {
                                console.log(data)
                                dataFromCallbackTwo(data[0]["id"], (err, data) => {
                                    if (err) {
                                        console.log(err)
                                    } else {
                                        console.log(data)
                                        let mindData = data.filter((power) => {
                                            return power.name === "Mind";
                                        })[0]["id"];
                                        dataFromCallbackthree(mindData, (err, data) => {
                                            if (err) {
                                                console.log(err)
                                            } else {
                                                console.log(data)
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }, 2 * 1000)
    
}
module.exports = problem4


